# Image Segmentation Keras (Beau Hobba Version) : Implementation of Segnet, FCN, UNet, PSPNet and other models in Keras.

Increased implementation of models/code described in 
 https://divamgupta.com/image-segmentation/2019/06/06/deep-learning-semantic-segmentation-keras.html.

Credit to the base code goes to: 

Divam Gupta : https://divamgupta.com [![Twitter](https://img.shields.io/twitter/url.svg?label=Follow%20%40divamgupta&style=social&url=https%3A%2F%2Ftwitter.com%2Fdivamgupta)](https://twitter.com/divamgupta)
[Rounaq Jhunjhunu wala](https://github.com/rjalfa)


This new code was created by: 
BEAU HOBBA of University of Sydney 

## Change-Log: 

### 10/10/2020 
* Fixed up classification plots a lot!
* Added a place to save classification plots in (classification_save_location)
* some more general fixes to code to make it more improved
* Small Example of the new fixes

```python
import keras
optimizer = tf.keras.optimizers.Adagrad()
import numpy as np

opt = keras.optimizers.Adagrad(learning_rate=0.0032)

weights = [1, 1, 338, 1685, 49, 1, 1, 1, 1, 1]
#weights = [1, 1, 204.561, 17.566, 132.8758, 1.5, 89.132, 1.5, 47.238, 679.93]

plot_location = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/plots/"
classification_location = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/classification/"
class_names = ["None", "Grass", "Dirt", "Other", "Thistle", "Deadthistle", "Pattersonscurse", "Water", "Stingingnettle", "capeweed"]

model.train(
    train_images =  "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/dataset7/images_prepped_train",
    train_annotations = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/dataset7/annotations_prepped_train",
    checkpoints_path = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/checkpoints19/vggunet" , epochs=2,
    val_images = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/dataset7/images_prepped_validation",
    val_annotations = "/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/dataset7/annotations_prepped_validation",
    csv_file_path ="/content/drive/My Drive/Semantic/SEMANTIC/SemanticSegment/logs/quickrun2.log",
    steps_per_epoch=528/2/2/2, input_height=224, input_width=224, optimizer_name=opt, do_augment=True, batch_size = 2, verify_dataset=False, validate=True,
    cost_matrix = weights, loss_type=0, plot_metrics=True, plot_metrics_save_location=plot_location,
    label_names=class_names, classification_reports=True , save_plot_metrics=True, gen_use_multiprocessing=True, callback_dynamic_on=True,
    classification_save_location=classification_location
    )
)
```	

This will make confusion matrix and classification report every epoch. It will save these in classification location.
Without callback_dynamics this will just happen at the end. This should be good enough for any thesis with the evaluations this provides!
If you need the files later a convient csv file is stored for each :) 


### 9/10/2020 
* Added metrics:

```python
plot_metrics=True, plot_metrics_save_location="", save_plot_metrics=True
)
```

Adds nice plots of the accuracy, loss, f1 and iou for the model. Can specify if you want to save them and the location of where you want to save them. 



* Added Classification plots:
```python
class_names = ["None", "Grass", "Dirt", "Thistle", "Deadthistle", "Pattersonscurse", "Water", "Stingingnettle", "capeweed"]


label_names=class_names, classification_report=True , callback_dynamic_on=True
)
```
* Can now add confusion matrices and classification reports for each class. If you input a class name list it will not use IDS. Turn on classification_reports for this. 
* If you want to create a plot for each epoch, then callback_dyanmic_on can be set to true. NOTE: This only works on systems which allow multiprocessing (eg: Google Colab).  


:) 